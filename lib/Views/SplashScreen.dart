import 'package:flutter/material.dart';
import 'package:oraan_assessment/Constants/ColorConstants.dart';
import 'package:oraan_assessment/Views/LoginScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    new Future.delayed(
        const Duration(seconds: 3),
        () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginScreen()),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants().themeColor,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 6),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/Logo.png",
              height: MediaQuery.of(context).size.height / 2.5,
            ),
            Text(
              "Save money and money\nwill save you",
              style: TextStyle(
                  fontFamily: "Acriform",
                  color: ColorConstants().whiteColor,
                  fontSize: 22),
            ),
          ],
        ),
      ),
    );
  }
}
