import 'package:flutter/material.dart';
import 'package:oraan_assessment/Constants/ColorConstants.dart';
import 'package:oraan_assessment/NetworkCalls/NetworkCalls.dart';
import 'package:oraan_assessment/Views/DashBoardAndTabs/DashBoard.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController phnNmbrController = new TextEditingController(text: "");
  TextEditingController passwordController =
      new TextEditingController(text: "");

  final _formKey = GlobalKey<FormState>();
  int state = 0;
  bool isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants().whiteColor,
      appBar: AppBar(
        backgroundColor: ColorConstants().whiteColor,
        elevation: 0,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: Icon(
                        Icons.arrow_back_ios,
                        size: 15,
                      )),
                  Padding(padding: EdgeInsets.only(left: 13)),
                  Text(
                    "Welcome Back.",
                    style: TextStyle(
                        fontFamily: "NotoSansHK",
                        fontSize: 25,
                        fontWeight: FontWeight.w500),
                  )
                ],
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    new TextFormField(
                      keyboardType: TextInputType.phone,
                      controller: phnNmbrController,
                      decoration: new InputDecoration(
                        enabledBorder: const OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 1.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorConstants().themeColor,
                                width: 2.0)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorConstants().redColor, width: 2.0)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorConstants().redColor, width: 2.0)),
                        hintText: 'Enter Your Phone Number',
                        labelText: 'Phone Number',
                      ),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Please Enter Phone Number!";
                        }
                        if (val.length != 11) {
                          return "Please enter a valid phone number";
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    new TextFormField(
                      controller: passwordController,
                      obscureText: isObscure,
                      decoration: new InputDecoration(
                        enabledBorder: const OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 1.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorConstants().themeColor,
                                width: 2.0)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorConstants().redColor, width: 2.0)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorConstants().redColor, width: 2.0)),
                        hintText: 'Enter Your Password',
                        labelText: 'Password',
                        suffixIcon: GestureDetector(
                          child: isObscure == true
                              ? Icon(Icons.visibility_off)
                              : Icon(Icons.visibility),
                          onTap: () {
                            if (isObscure) {
                              isObscure = false;
                            } else {
                              isObscure = true;
                            }
                            setState(() {});
                          },
                        ),
                      ),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Please Enter Password!";
                        }
                        if (val.length < 4) {
                          return "Password should not be less tan 4";
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          state = 1;
                          setState(() {});
                          NetworkCals()
                              .loginRequest(phnNmbrController.text,
                                  passwordController.text)
                              .then((value) {
                            state = 0;
                            setState(() {});
                            if (value.data.message.contains("user not found")) {
                              showAlertDialog(context);
                            } else {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DashBoard(
                                          userID: value.data.userId)));
                            }
                          });
                        }
                      },
                      child: Container(
                        height: 40,
                        width: 80,
                        padding: EdgeInsets.symmetric(vertical: 5),
                        decoration: BoxDecoration(
                            color: ColorConstants().themeColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                        child: Center(
                          child: state == 0
                              ? Text(
                                  "Login",
                                  style: TextStyle(
                                      fontFamily: "NotoSansHK",
                                      color: ColorConstants().whiteColor),
                                )
                              : Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0, horizontal: 30),
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.white),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    Text(
                      "Forgot Password?",
                      style: TextStyle(
                          fontFamily: "NotoSansHK",
                          color: state == 0
                              ? ColorConstants().themeColor
                              : ColorConstants().greyColor,
                          fontSize: 15),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("This User Is Not Registered!"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
