import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:oraan_assessment/Constants/ColorConstants.dart';
import 'package:oraan_assessment/Views/DashBoardAndTabs/HomeTab.dart';

class DashBoard extends StatefulWidget {
  int userID;
  DashBoard({@required this.userID});
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(widget.userID);
    TextStyle optionStyle =
        TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
    final List<Widget> _widgetOptions = <Widget>[
      HomeTab(
        userID: widget.userID,
      ),
      Text(
        'Committees',
        style: optionStyle,
      ),
      Text(
        'Pay Now',
        style: optionStyle,
      ),
      Text(
        'Transactions',
        style: optionStyle,
      ),
      Text(
        'Settings',
        style: optionStyle,
      ),
    ];
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/cmts.png",
              height: 26,
            ),
            title: Text('Commitees'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle_outline),
            title: Text('Pay Now'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            title: Text('Transaction'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
        ],
        currentIndex: _selectedIndex,
        unselectedItemColor: Colors.grey[400],
        selectedItemColor: ColorConstants().activeIconColor,
        onTap: _onItemTapped,
        backgroundColor: ColorConstants().whiteColor,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
    );
  }
}
