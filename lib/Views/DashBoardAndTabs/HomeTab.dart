import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:oraan_assessment/Constants/ColorConstants.dart';
import 'package:oraan_assessment/Models.dart/SavingsModel.dart';
import 'package:oraan_assessment/NetworkCalls/NetworkCalls.dart';

class HomeTab extends StatelessWidget {
  final int userID;
  HomeTab({@required this.userID});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: ColorConstants().lightGrey,
        child: Column(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(
                  top: 20,
                  bottom: 10,
                ),
                width: MediaQuery.of(context).size.width,
                color: ColorConstants().darkGrey,
                child: Column(
                  children: <Widget>[
                    Container(
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.white, width: 2)),
                        child: Center(
                            child: Image.asset(
                          "assets/Avatar.png",
                          height: 80,
                        ))),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    Text(
                      "Juhi Jafferii",
                      style: GoogleFonts.notoSans(
                          color: ColorConstants().whiteColor),
                    )
                  ],
                )),
            Padding(padding: EdgeInsets.only(bottom: 20)),
            Image.asset(
              "assets/gold.png",
              height: 50,
            ),
            Text(
              "Your patience and discipline is paying off!!",
              style: GoogleFonts.notoSans(fontSize: 15),
            ),
            Text(
              "Lifetime savings",
              style: GoogleFonts.notoSans(
                  fontSize: 18, fontWeight: FontWeight.w500),
            ),
            // Text(
            //   "PKR 415,000",
            //   style: TextStyle(
            //       fontSize: 30,
            //       fontWeight: FontWeight.w500,
            //       fontFamily: "NotoSansHK"),
            // ),
            FutureBuilder<SavingsModel>(
              future: NetworkCals().getSavings(userID.toString()), // async work
              builder:
                  (BuildContext context, AsyncSnapshot<SavingsModel> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return CircularProgressIndicator();
                  default:
                    if (snapshot.hasError)
                      return Text('Error: ${snapshot.error}');
                    else
                      return Text(
                        "PKR " + snapshot.data.data.lifeTimeSavings,
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w500,
                            fontFamily: "NotoSansHK"),
                      );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
