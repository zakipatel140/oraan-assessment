class UrlConstants {
  static String baseURL = "https://naya-oraan.herokuapp.com/";
  static String loginURL = baseURL + "users/login";
  static String savingsAPIURL = baseURL + "installment/get-by-userid?user_id=";
}
