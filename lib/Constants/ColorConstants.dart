import 'package:flutter/material.dart';

class ColorConstants {
  Color themeColor = Color(0xFF50C4CC);
  Color whiteColor = Colors.white;
  Color redColor = Colors.red;
  Color greyColor = Color(0xFFaeb9be);
  Color darkGrey = Color(0xff2D4654);
  Color lightGrey = Color(0xFFf4f6f8);
  Color activeIconColor = Color(0xFF2B4655);
}
