import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:oraan_assessment/Constants/UrlConstants.dart';
import 'package:oraan_assessment/Models.dart/LoginModel.dart';
import 'package:oraan_assessment/Models.dart/SavingsModel.dart';

class NetworkCals {
  Future<LoginModel> loginRequest(
    String phoneNumber,
    String password,
  ) async {
    var body = jsonEncode({"userPassword": password, "userPhone": phoneNumber});
    print(body);
    try {
      final response = await http.post(UrlConstants.loginURL,
          headers: {
            "Content-Type": "application/json",
          },
          body: body);
      if (response.statusCode < 200 ||
          response.statusCode > 400 ||
          json == null) {
        LoginModel user = LoginModel();
        user.message = "Technical Error";
        user.data = null;
        return Future.value(user);
      }
      return LoginModel.fromJson(json.decode(response.body));
    } catch (e) {
      LoginModel user = LoginModel();
      user.message = e;
      user.data = null;
      return Future.value(user);
    }
  }

  Future<SavingsModel> getSavings(String userID) async {
    try {
      var response = await http.get(
        UrlConstants.savingsAPIURL + userID,
        headers: {
          "Content-Type": "application/json",
        },
      );
      if (response.statusCode < 200 ||
          response.statusCode > 400 ||
          json == null) {
        SavingsModel savings = SavingsModel();
        savings.message = "Technical Error";
        savings.data = null;
        return Future.value(savings);
      }
      return SavingsModel.fromJson(json.decode(response.body));
    } catch (e) {
      SavingsModel savings = SavingsModel();
      savings.message = e;
      savings.data = null;
      return Future.value(savings);
    }
  }
}
