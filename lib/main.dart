import 'package:flutter/material.dart';
import 'package:oraan_assessment/Constants/ColorConstants.dart';
import 'package:oraan_assessment/Views/SplashScreen.dart';

void main() {
  runApp(OraanApp());
}

class OraanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: ColorConstants().themeColor,
      ),
      home: SplashScreen(),
    );
  }
}
