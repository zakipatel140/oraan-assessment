import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    this.status,
    this.message,
    this.data,
  });

  int status;
  dynamic message;
  Data data;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.userId,
    this.message,
  });

  int userId;
  String message;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userId: json["userId"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "message": message,
      };
}
