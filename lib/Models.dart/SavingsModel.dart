// To parse this JSON data, do
//
//     final savingsModel = savingsModelFromJson(jsonString);

import 'dart:convert';

SavingsModel savingsModelFromJson(String str) =>
    SavingsModel.fromJson(json.decode(str));

String savingsModelToJson(SavingsModel data) => json.encode(data.toJson());

class SavingsModel {
  SavingsModel({
    this.status,
    this.message,
    this.data,
  });

  int status;
  dynamic message;
  Data data;

  factory SavingsModel.fromJson(Map<String, dynamic> json) => SavingsModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.userId,
    this.lifeTimeSavings,
  });

  String userId;
  String lifeTimeSavings;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userId: json["userId"],
        lifeTimeSavings: json["lifeTimeSavings"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "lifeTimeSavings": lifeTimeSavings,
      };
}
