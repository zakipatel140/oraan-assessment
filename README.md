# Oraan Assessment

**Version 1.0.0**

This repo was created to demonstrate the assessment.

---

## Contributors

- Muhammad Muzammil Khan <mzmkhn123@gmail.com>

---

## License & Purpose

This Repo is developed by flutter and android developer Muhammad Muzammil Khan for assessment purposes for a company called Oraan.

---

## Content 

This repo contains a flutter project which contains following elements:

* Multiple screen designs. 
* 2 apis integration. (Network Calls)
* Color Constants.
* Material Design.
* Custom fonts.
* 3rd party libraries. 

---

## To Run Project

1. To run this project you need to install flutter development environment.
2. Clone this project.
3. Run command "flutter pub get" on terminal, to get all packages.
4. Run command "flutter run" on the terminal, to run on a device or emulator.
5. You can create a release apk for android with command "flutter build apk --release" on terminal.